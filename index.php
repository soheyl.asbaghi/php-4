<?php namespace MyExample;

// TODO: <<Errors>>

// very error that PHP generates includes a type:
/* <- Error Types -->
 * 1.Syntax Errors
 * 2.Fatal Errors
 * 3.Warning
 * 4.Notice Errors
 */

// <- Handling errors -->

//Handling errors with PHP
// error_reporting();

//User error handler function
//Using the die() function

/*$file = fopen("welcome.txt", "r");

if (!file_exists("welcome.txt")) {
die("File not found");
} else {
$file = fopen("welcome.txt", "r");
}
 */

//Custom error handler
//error_function (error_level, error_message, error_file, error_line, error_context);

/*function customError($errno, $errstr)
{
echo "<b>Error:</b> [$errno] $errstr";
echo "Web master has been notified";
//send error email for user
error_log("Error: [$errno] $errstr", 1, " someone@example.com ", " From: webmaster@example.com ");
}

//set error handler
set_error_handler("customError", E_USER_WARNING);

//trigger error
echo ($test);

//trigger error
$test = 20;

if ($test > 10) {

trigger_error("Value must be 10 or below", E_USER_WARNING);

}
 */

// TODO: <<Exception>>
/*
Exception() -> The contractor of the Exception object
getCode() -> Returns the exception code
getFile() -> Returns the full path of the file in which the exception was thrown
getMessage() -> Returns a string describing why the exception was thrown
getLine() -> Returns the line number of the line of code which threw the exception
getPrevious() -> If this exception was triggered by another one, this method returns the previous exception. If not, then it returns null
getTrace() -> Returns an array with information about all of the functions that were running at the time the exception was thrown
getTraceAsString() ->  Returns the same information as getTrace(), but in a string
 */

/*try {
throw new Exception();
$name = "John Doe";

} catch (Exception $e) {
var_dump("Error Message: " . $e->getCode());
}

class MyException extends Exception
{}

class Test
{
public function testing()
{
try {
try {
throw new MyException('foo!');
} catch (MyException $e) {
// rethrow it
throw $e;
}
} catch (Exception $e) {
var_dump($e->getMessage());
}
}
}

$foo = new Test;
$foo->testing();
 */

// TODO: <<Generators>>
// Generators provide an easy way to implement simple iterators without the overhead or complexity of implementing a class that implements the Iterator interface.

/*
function makeArray()
{
$array = [];
for ($i = 0; $i < PHP_INT_MAX; $i++) {
array_push($array, $i);
}
return $array;
}

foreach (makeArray() as $number) {
echo $number . PHP_EOL;
}
 */

function makeArray()
{
    $array = [];
    for ($i = 0; $i < 5; $i++) {
        yield $i;
    }
}
var_dump(makeArray()); //object(Generator)#1 (0)

foreach (makeArray() as $number) {
    echo $number . PHP_EOL . "<br/>";
} // result => 0 1 2 3 4

$arrayFun = makeArray();
echo $arrayFun->current() . "<br/>";
$arrayFun->next();
echo $arrayFun->current() . "<br/>";

// return value;
function myGen()
{
    yield 1;
    yield 2;

    return 3;
}

$gen = myGen();

foreach ($gen as $value) {
    echo $value . "<br/>";
}

echo $gen->current();
echo $gen->getReturn() . "<br/>";

// send value to gen;
function myGenTwo()
{
    echo yield "bye PHP";
}

// $genTwo = myGenTwo();

// echo $genTwo->current() . PHP_EOL;
// echo $genTwo->send("Hello PHP") . PHP_EOL;

// TODO: <<Attributes>>
// attributes: they are meant to add meta data to classes and methods, nothing more
use Attribute;

#[Attribute]
class ListensTo
{
    private $event;

    public function __construct(string $event)
    {
        $this->event = $event;
    }
}
